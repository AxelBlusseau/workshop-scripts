# Data Gen / Database Insert

### Files

- data_insert.py => Script d'insertion des données dans la table prediction
- data-gen.py => Script qui permet de créer des datas
- prediction_table.sql => Script sql qui permet de créer la table prediction 

### Before execute data_insert.py script

Installez les packets suivant :
> pip install pandas
> pip install psycopg2

Et exécutez le script sql avant.
Faites attention au datasource et aux données des Lignes contenues dans la bdd => Le script marche par défaut avec les id de Lignes 1, 2, 3 (comme les lignes de tram dans la vraie vie)
