import psycopg2
import pandas as pd
import sys

#JE PARS DU PRINCIPE QU ON A 3 LIGNES INSEREES EN BDD

# Reset the pgAdmin seq
# TRUNCATE TABLE scheme.table RESTART IDENTITY CASCADE;


# Open the Workbook

insertPrediction = 'INSERT INTO public.prediction ("idLigne", prediction, date) VALUES (%s, %s, %s)'
num_tram_lignes = [1, 2, 3] #Ligne de tram de la TAN (correspond à idLigne)

try:
    connection = psycopg2.connect(
        user="postgres", password="admin", host="127.0.0.1", port="5432", database="workshop")
    cursor = connection.cursor()

    for idLigne in num_tram_lignes:
        datas = pd.read_csv("ai_prediction_l" + str(idLigne) + ".csv")

        for index, row in datas.iterrows():
            pred = int(row['pred_nbCustomers']) if int(row['pred_nbCustomers']) != 1 else 0
            cursor.execute(insertPrediction, [idLigne, pred, row['date']])
            connection.commit()

except (Exception, psycopg2.Error) as error:
    if(connection):
        print("Failed to insert record into prediction table : ", error)

finally:
    # closing database connection.
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")