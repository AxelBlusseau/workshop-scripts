import csv
import datetime
import random

def datagenerate(records, headers):
    with open("dataset_futur_l3.csv", mode='w', newline='') as csvFile:
        writer = csv.writer(csvFile, delimiter=';')
        writer.writerow(headers)

        currentDate = datetime.datetime(2021, 1, 1, 0, 0, 0) #Date de début
        end_date = datetime.datetime(2021, 12, 31, 23, 0, 0) #Date de fin
        end_date_plus = datetime.datetime(2021, 10, 30, 20, 0, 0) #Date qui sert à rajouter des valeurs nulls
        timeDelta = datetime.timedelta(hours=1) #Delta heure
        while currentDate <= end_date:
            #Management
            date = currentDate

            delta_year = ( currentDate.year - 2000 ) * 100 + 100
            delta_hour = None
            if currentDate.hour == 10 or (currentDate.hour >= 12 and currentDate.hour <= 14) or (currentDate.hour >= 17 and currentDate.hour <= 18) : 
                delta_hour = delta_year * 0.8
            else :
                delta_hour = delta_year * 0.2

            if currentDate.hour >= 10 and currentDate.hour <= 20:
                nbCustomers = random.randint(int(delta_hour), int(delta_year))
            else:
                nbCustomers = 0

            action = 0  

            if nbCustomers <= 50 and nbCustomers != 0:
                action = -2
            elif nbCustomers > 50 and nbCustomers < 200:
                action = -1
            elif nbCustomers > 600 and nbCustomers < 800:
                action = 1
            elif nbCustomers > 800 and nbCustomers < 1000:
                action = 2
            elif nbCustomers > 1000:
                action = 3

            writer.writerow([currentDate, nbCustomers, action])

            currentDate += timeDelta

        #while currentDate <= end_date_plus:
        #    writer.writerow([currentDate, 0, 0])
        #    currentDate += timeDelta
    
if __name__ == '__main__':
    records = 10000
    headers = ["date", "nbCustomers", "action"]
    datagenerate(records, headers)
    print("CSV generation complete!")