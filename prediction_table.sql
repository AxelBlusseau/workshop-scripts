-- Table: scheme.prediction

DROP TABLE public.prediction;

-- NE PAS OUBLIER DE CHANGER LE SCHEMA SI C'EST LE MAUVAIS

CREATE TABLE public.prediction
(
    "idPrediction" integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    "idLigne" integer,
    "prediction" integer,
    "date" timestamp,
    CONSTRAINT from_ligne FOREIGN KEY ("idLigne")
        REFERENCES public."Ligne" ("id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
)

